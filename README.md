# Install all dependencies
```
yum update -y
rpm -ivh rpmforge-release-...

Installation of MySQL, Apache and PHP
yum install MariaDB-server MariaDB-client mysql-devel -y
chkconfig mysqld on
yum install -y httpd-manual
chkconfig httpd on
yum install -y ksh perl-Archive-Zip perl php-cli php perl-IO-Socket-SSL

Installation of some libraries:
yum groupinstall "Server Configuration Tools"
yum install net-snmp* bind-utils redhat-lsb giflib php-mysql php-mbstring graphviz -y

# Installation of specifics libraries:
yum install -y libmcrypt directfb cairo perl-rrdtool libtool-ltdl mrtg net-snmp-perl net-snmp-utils perl-XML-LibXML-Common perl-XML-SAX rrdtool ImageMagick netpbm-progs gd-progs

#Installation of Opsview 3.0.2
#Download all Opsview packages from http://downloads.opsview.org/ and exec:
rpm -Uvh opsview-3.0.2.2191-1.ct5.noarch.rpm opsview-core-3.0.2.2191-1.ct5.noarch.rpm opsview-perl-3.0.1.130-1.ct5.i386.rpm opsview-web-3.0.2.2191-1.ct5.noarch.rpm opsview-base-3.0.2.2191-1.ct5.i386.rpm opsview-reports-2.2.1.238-1.ct5.noarch.rpm

```

# Configure database
Configuration of MySQL (MariaDB 10.0)
Set boot time start and change the root password of the MySql server:
```
chkconfig mysqld on
service mysqld start

echo "update mysql.user set password=PASSWORD('abcd@1234') where user='root';" | mysql -uroot

Here we have set the root password to abcd@1234.
You need to edit the Opsview configuration file /usr/local/nagios/etc/opsview.conf to change the default passwords.

To create users, rights, databases and tables, type:
/usr/local/nagios/bin/db_mysql -u root -pabcd@1234
/usr/local/nagios/bin/db_opsview db_install
/usr/local/nagios/bin/db_runtime db_install
/usr/local/nagios/bin/db_odw db_install
/usr/local/nagios/bin/db_reports db_install

Gen nagios config 
/usr/local/nagios/bin/rc.opsview gen_config

Ref:
http://romain.novalan.fr/wiki/OpsView_3.0.2_CentOS_5.2
http://www.yongbok.net/blog/how-to-install-opsview-on-centos/
```

# Account information
```
account default:
ID: admin
password: initial
```
# Write script plugins
```
Write a file bash shell or python...
tutorial: https://assets.nagios.com/downloads/nagioscore/docs/nagioscore/3/en/pluginapi.html

- move script to /usr/local/nagios/libexec/
- go to setting on web UI 
- services check 
- add new 
- fill name and choose correct script on server 
- then move it into services group. DONE
```